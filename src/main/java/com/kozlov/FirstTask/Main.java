package com.kozlov.FirstTask;

import com.kozlov.FirstTask.View.MyView;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

  public static void main(String[] args) throws FileNotFoundException {
        new MyView().show();
//    test1();
//    test2();
  }

  static void test1() {
    String pattern = "[a-z]+";
    String text = "Now is the time";
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(text);
    while (m.find()) {
      System.out.print(text.substring(m.start(), m.end()) + "*");
      System.out.print(m.end());
    }
    System.out.print("\nlength="+text.length());
  }

  static void test2() {
//    String pattern = "<.*>";
    String pattern = "<[^>]*>";
    String text = "<p><body>Beginning with bold text</b> next, text body,<i>italic text</i> end of text.</p>";
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(text);
    while (m.find()) {
      System.out.print(text.substring(m.start(), m.end()) + "*");
    }
  }

}
