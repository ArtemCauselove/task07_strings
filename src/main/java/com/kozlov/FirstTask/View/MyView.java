package com.kozlov.FirstTask.View;

import com.kozlov.FirstTask.tasks.StringUtils;

import java.util.*;

public class MyView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("7", bundle.getString("7"));
    menu.put("Q", bundle.getString("Q"));
  }

  public MyView() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::testStringUtils);
    methodsMenu.put("2", this::internationalizeMenuUkrainian);
    methodsMenu.put("3", this::internationalizeMenuEnglish);
    methodsMenu.put("4", this::internationalizeMenuHolland);
    methodsMenu.put("5", this::internationalizeMenuFrench);
    methodsMenu.put("6", this::testRegEx);
    methodsMenu.put("7", this::test3);
  }

  private void testStringUtils() {
    StringUtils utils = new StringUtils();
    System.out.println(utils.concate(1," example",2,"time"));
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
  private void internationalizeMenuHolland() {
    locale = new Locale("nl");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
  private void internationalizeMenuFrench() {
    locale = new Locale("fr");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void testRegEx() {
    String message = "I saw you eating the watermelon";
    System.out.println(message);
    String message2 = message.replaceAll("[aeiou]", "_");
    System.out.println(message2);
    String[] mm = message.split("the|you");
    for (String str : mm) {
      System.out.println(str);
    }
  }

  private void test3() {

    String s1 = "Cat";
    String s2 = "Cat";
    String s3 = new String("Cat");
    String s4 = s3.intern();

    System.out.println("s1 == s2 :"+(s1==s2));
    System.out.println("s1 == s3 :"+(s1==s3));

    System.out.println(System.identityHashCode(s1));
    System.out.println(System.identityHashCode(s2));
    System.out.println(System.identityHashCode(s3));
    System.out.println(System.identityHashCode(s4));
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.println(menu.get(key));
      }
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
