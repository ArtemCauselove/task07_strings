package com.kozlov.SecondTask.Model;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Model {
    String pattern = "[?]+";
    Pattern p = Pattern.compile(pattern);
    public int SentenceWithEqualWords(String text) {
        final List<String> sentences = TextToSentences(text);
        String[] CurrentSentence;
        ArrayList<Integer> maxSentence = new ArrayList<>();
        for (int k = 0; k < sentences.size(); k++) {
            int count = 0;
            CurrentSentence = sentences.get(k).split(" ");
            ArrayList<String> listOfFrequency = new ArrayList<>(Arrays.asList(CurrentSentence));
            for (int i = 0; i < CurrentSentence.length; i++) {
                if (Collections.frequency(listOfFrequency, CurrentSentence[i]) > 1) {
                    count = count + Collections.frequency(listOfFrequency, CurrentSentence[i]);
                }
            }
            maxSentence.add(count);
        }
        int maxIndex = maxSentence.indexOf(Collections.max(maxSentence));
        return maxIndex;
    }

    public List<String> getAllByIncreasingWords(String text) {
        List<String> sentences = TextToSentences(text);
        sentences.sort(Comparator.comparingInt(s -> s.split("\\s+").length));
        return sentences;
    }

    public String findUniqueInFirst(String text) {
         List<String> sentences = TextToSentences(text);
        String[] FirstSentence;
        String[] NextSentence;
        String unique = "";
        boolean isUnique = true;
        FirstSentence = sentences.get(0).split(" ");
        for (int i = 0; i < FirstSentence.length; i++) {
            for (int k = 1; k < sentences.size(); k++) {
                NextSentence = sentences.get(k).split(" ");
                for (int n = 0; n < NextSentence.length; n++) {
                    if (FirstSentence[i].equals(NextSentence[n])) {
                        isUnique = false;
                        break;
                    }
                }
                if (!isUnique) {
                    break;
                }
            }
            if (isUnique) {
                unique = FirstSentence[i];
            }
            isUnique = true;
        }
        return unique;
    }

    public Set<String> WordForLengthInQuestions(String text, int length) {
        List<String> sentences = TextToSentences(text);
        Set<String> wordsInQuestions = new HashSet<>();
        for (int i = 0; i < sentences.size(); i++) {
            Matcher m = p.matcher(sentences.get(i));
            if (m.find()) {
                String[] CurrentSentence;
                CurrentSentence = sentences.get(i).split(" ");
                for(int k=0; k < CurrentSentence.length; k++){
                    if(CurrentSentence[k].length()==length){
                        wordsInQuestions.add(CurrentSentence[k]);
                    }
                }
            }
        }
        return wordsInQuestions;
    }

    public List<String> getSortedByFirstLetter(String text) {
        List<String> sentences = TextToSentences(text);
        List<String> changed = new ArrayList<>();
        final List<String> sorted = sentences.stream()
                .map(s -> s.split(" "))
                .flatMap(Arrays::stream)
                .sorted(Comparator.comparingInt(s -> (int) s.charAt(0)))
                .collect(Collectors.toList());
        changed.add(sorted.get(0));
        for (int i = 0; i < sorted.size() - 1; i++) {
            if (sorted.get(i)
                    .charAt(0) != sorted.get(i + 1)
                    .charAt(0)) {
                changed.add("\n   " + sorted.get(i + 1));
            } else {
                changed.add(sorted.get(i + 1));
            }
        }
        return changed;
    }

    public String findBiggestPalindrom(String text){
        final List<String> sentence = TextToSentences(text);
        boolean palindrome = true;
        List<String> palindroms = new ArrayList<>();
        for (int i = 0; i < sentence.size(); i++) {
            String[] Current;
            Current = sentence.get(i).split(" ");
            for(int c = 0; c<Current.length;c++) {
                for (int k = 0; k < Current[c].length(); k++) {
                    palindrome = false;
                    if (Current[k].equals(Current[Current[c].length()-1-k])) {
                        palindrome = true;
                    }
                }
            }
            if(palindrome){
                palindroms.add(Current[i]);
            }
        }
        String maxPalindrome = Collections.max(palindroms);
        return maxPalindrome;
    }

    public List<String> TextToSentences(String text){
        List<String> sentences = new ArrayList<>();
       StringBuilder  sentence = new StringBuilder();
        for(int i = 0; i<text.length();i++){
            sentence.append(text.charAt(i));
            if(text.charAt(i) == '.' || text.charAt(i) == '?' || text.charAt(i) == '!'){
                sentences.add(sentence.toString());
                sentence = new StringBuilder();
            }
        }
        return sentences;
    }
}
