package com.kozlov.SecondTask.Controller;

import com.kozlov.SecondTask.Model.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Reader {
    public String reader() {
        String fileName = "D:\\EPAM\\task07_Strings\\example.txt";
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName), Charset.forName("UTF-8"))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        String text = fixText(sb.toString());
        return text;
    }
    private String fixText(String text) {
        return text.replaceAll("([\\t ]+)", " ");
    }

    public static void main(String[] args) {

    }
}
