package com.kozlov.SecondTask.Controller;

import com.kozlov.SecondTask.Model.Model;

import java.util.List;
import java.util.Set;

public class Controller {
    Reader reader = new Reader();
    Model model = new Model();
    String text = reader.reader();
    public int SentenceWithEqualWords(){
        return model.SentenceWithEqualWords(text);
    }
    public List<String> getAllByIncreasingWords(){
        return model.getAllByIncreasingWords(text);
    }
    public String findUniqueInFirst(){
        return model.findUniqueInFirst(text);
    }
    public Set<String> WordForLengthInQuestions(int length){
        return model.WordForLengthInQuestions(text,length);
    }
}
