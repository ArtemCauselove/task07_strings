package com.kozlov.SecondTask.View;

import com.kozlov.SecondTask.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<Integer, String> menu = new HashMap<>();
    private Map<String, Printable> methodsMenu = new HashMap<>();
    private Controller controller = new Controller();
    private Scanner scan = new Scanner(System.in, "UTF-8");
    private static Logger logger = LogManager.getLogger(View.class);
    public View() {
        menu.put(0, "Find more count of sentences with similar words.\n");
        menu.put(1, "Out all sentences by increaing all words.\n");
        menu.put(2, "Find word in first sentence that is absent in other.\n");
        menu.put(3, "Out words given length in questions.\n");
        menu.put(4, "Exit\n");
        methodsMenu.put("0", this::outMoreSimilarSentences);
        methodsMenu.put("1", this::outByIncreasingWords);
        methodsMenu.put("2", this::outExceptWord);
        methodsMenu.put("3", this::outQuestionsWords);
        methodsMenu.put("4", this::quit);
    }
    public void show() {
         String keyMenu;
        do {
            showInfo();
            logger.info("Please, select menu point.\n");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
                logger.info("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
    private void quit() {
        logger.info("Exit\n");
    }
    private void showInfo() {
        logger.info("Menu for TextTask\n");
        menu.forEach((key, elem) -> logger.info(key + " : " + elem));
    }
    private void outMoreSimilarSentences() { logger.info(controller.SentenceWithEqualWords()); }
    private void outByIncreasingWords() {
        controller.getAllByIncreasingWords()
                .forEach(logger::info);
    }
    private void outExceptWord() {
        logger.info(controller.findUniqueInFirst());
    }
    private void outQuestionsWords() {
        logger.info("Please, input length of words in questions.\n");
        if (scan.hasNextInt()) {
            int length = scan.nextInt();
            controller.WordForLengthInQuestions(length)
                    .forEach(logger::info);
        } else {
            String logMessage = "Used incorrect length\n";
            logger.info(logMessage);
        }
    }
}

